<?php

/**
 * @file
 * Displays configurable mobile detect block variants.
 */

$plugin = array(
  'title' => t('Mobile detect block variants'),
  'description' => t('Mobile detect block variants.'),
  'single' => TRUE,
  'content_types' => array('mobile_detect_variants_blocks'),
  'render callback' => 'mobile_detect_variants_blocks_render',
  'category' => array(t('Mobile detect'), -9),
  'edit form' => 'mobile_detect_variants_blocks_edit_form',
);

/**
 * Ctools plugin render callback.
 */
function mobile_detect_variants_blocks_render($subtype, $conf, $args, $context) {
  // Initialize an empty block in case there is no selection.
  $object = new stdClass();
  $object->title = '';
  $object->content = '';

  // Display the first variant whose callback returns true.
  foreach (array_keys(mobile_detect_variants_get_variants()) as $callback) {
    if (!empty($conf[$callback]) && $callback()) {
      list($module, $delta) = explode(':', $conf[$callback]);
      $block = new stdClass();
      $block->module = $module;
      $block->delta = $delta;

      $array = module_invoke($block->module, 'block_view', $block->delta);
      // Allow modules to modify the block before it is viewed, via either
      // hook_block_view_alter() or hook_block_view_MODULE_DELTA_alter().
      drupal_alter(array('block_view', "block_view_{$module}_{$delta}"), $array, $block);

      $object->title = isset($array['subject']) ? $array['subject'] : '';
      $object->content = isset($array['content']) ? $array['content'] : '';
    }
  }

  return $object;
}

/**
 * Returns mobile detect variants.
 *
 * @return array
 *   An associative array of mobile detect variant titles, keyed by callback.
 */
function mobile_detect_variants_get_variants() {
  $variants = array(
    'mobile_detect_variants_callback_mobile' => t('Mobile'),
    'mobile_detect_variants_callback_not_mobile' => t('Not mobile'),
  );

  // Allow other modules to alter variants.
  drupal_alter('mobile_detect_variants', $variants);

  return $variants;
}

/**
 * Mobile detect variants callback for mobile.
 */
function mobile_detect_variants_callback_mobile() {
  $detect = mobile_detect_get_object();
  return $detect->isMobile();
}

/**
 * Mobile detect variants callback for not mobile.
 */
function mobile_detect_variants_callback_not_mobile() {
  return !mobile_detect_variants_callback_mobile();
}

/**
 * Ctools edit form constructor callback.
 */
function mobile_detect_variants_blocks_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  foreach (mobile_detect_variants_get_variants() as $callback => $title) {
    $form[$callback] = array(
      '#type' => 'select',
      '#title' => check_plain($title),
      '#options' => mobile_detect_variants_block_options(),
      '#default_value' => !empty($conf[$callback]) ? $conf[$callback] : '',
      '#empty_option' => t('- None -'),
    );
  }

  return $form;
}

/**
 * Gets a list of blocks.
 *
 * Note we aren't using _block_load_blocks() for the same reason as ctools
 * (because we don't want them sorted by region and they're not cached anyway).
 * However, we also aren't using _ctools_block_load_blocks(), becuase the
 * underscore delimeter ("{$block->module}_{$block->delta}") would make it
 * difficult to extract the module and delta in 
 * mobile_detect_variants_blocks_render() without looping over all implementing
 * modules unnecessarily.
 *
 * @return array
 *   An array of block info suitable for a select form element.
 */
function mobile_detect_variants_block_options() {
  if (!module_exists('block')) {
    return array();
  }

  $options = &drupal_static(__FUNCTION__, array());
  if (empty($options)) {
    $options = array();
    foreach (module_implements('block_info') as $module) {
      $blocks = module_invoke($module, 'block_info');
      if ($blocks) {
        foreach ($blocks as $delta => $block) {
          $options["$module:$delta"] = $block['info'];
        }
      }
    }
  }

  return $options;
}

/**
 * Submit handler for mobile_detect_variants_blocks_edit_form().
 */
function mobile_detect_variants_blocks_edit_form_submit($form, &$form_state) {
  foreach (element_children($form) as $key) {
    if (in_array($key, array_keys(mobile_detect_variants_get_variants()))) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}
