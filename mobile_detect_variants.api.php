<?php

/**
 * @file
 * Hooks and callbacks provided by the Mobile detect variants module.
 *
 * See @link https://drupal.org/node/1250500 Drupal 7 adopted policy for callback signatures. @endlink
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Implements hook_mobile_detect_variants_alter().
 */
function hook_mobile_detect_variants_alter(&$variants) {
  // Example: add tablet variant.
  $variants['mobile_detect_variants_callback_tablet'] = t('Tablet');

  // Example: remove 'not mobile' variant callback in favor of a custom-defined
  // 'desktop' variant.
  if (isset($variants['mobile_detect_variants_callback_not_mobile'])) {
    unset($variants['mobile_detect_variants_callback_not_mobile']);
  }
  $variants['mobile_detect_variants_callback_desktop'] = t('Desktop');
}

/**
 * @} End of "addtogroup hooks".
 */

/**
 * @addtogroup callbacks
 */

/**
 * Detects a tablet mobile detect variant.
 *
 * Callback for mobile_detect_variants_get_variants().
 *
 * @return bool
 */
function mobile_detect_variants_callback_tablet() {
  $detect = mobile_detect_get_object();
  return $detect->isTablet();
}

/**
 * Detects a custom-defined 'desktop' variant, as neither mobile nor tablet.
 *
 * Callback for mobile_detect_variants_get_variants().
 *
 * @return bool
 */
function mobile_detect_variants_callback_desktop() {
  return !mobile_detect_variants_callback_mobile() && !mobile_detect_variants_callback_tablet();
}

/**
 * @} End of "addtogroup callbacks".
 */
